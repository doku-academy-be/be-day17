package com.example.day17.controller;

import com.example.day17.model.AuthResponse;
import com.example.day17.model.Responses;
import com.example.day17.model.UserResponse;
import com.example.day17.services.TokenService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.net.http.HttpRequest;
import java.security.Principal;

@RestController
public class AuthController {
    private static final Logger LOG = LoggerFactory.getLogger(AuthController.class);
    @Autowired
    private TokenService tokenService;

//    public AuthController(TokenService tokenService){
//        this.tokenService=tokenService;
//    }

    @PostMapping("v2/auth/login")
    public Responses token(@RequestBody UserResponse user){
        LOG.debug("Token requested for user: '{}", user.phone());
        String token = tokenService.generateToken(user);
        LOG.debug("Token granted: {}", token);
        return new Responses(new AuthResponse(token));
    }

    @GetMapping("v2/auth/info")
    public ResponseEntity<String> user(Principal principal){
        String user = principal.getName();
        return ResponseEntity.ok("Phone: "+user);
    }

}
