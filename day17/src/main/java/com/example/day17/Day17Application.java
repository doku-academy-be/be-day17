package com.example.day17;

import com.example.day17.securities.RsaKeyProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties(RsaKeyProperties.class)
public class Day17Application {

	public static void main(String[] args) {
		SpringApplication.run(Day17Application.class, args);
	}

}
